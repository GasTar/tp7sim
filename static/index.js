let table;
// let data_row_modal = {
//     autos1: [],
//     autos2: [],
//     autos_en_viaje: [],
// }

$(document).ready( function () {
    let c_simulaciones = document.getElementById('cantidad_iteraciones');
    let mostrar_desde = document.getElementById('mostrar_desde');
    let c_filas_mostrar = document.getElementById('c_filas_mostrar');
    let btn_simular = $('#btn_simular');
    let capacidad_vagon = document.getElementById('capacidad_vagon');
    let ganancia_traslado_auto = document.getElementById('ganancia_traslado_auto');
    let costo_traslado_vagon = document.getElementById('costo_traslado_vagon');
    let tolerancia_espera = document.getElementById('tolerancia_espera');
    let costo_abandono = document.getElementById('costo_abandono');
    let reposicion_combustible_r = document.getElementById('reposicion_combustible');
    let tiempo_carga_combustible = document.getElementById('tiempo_carga_combustible');
    let tiempo_vagon_vacio = document.getElementById('tiempo_vagon_vacio');
    let txt = $('#txt_cant_autos_traslado');
    let radio_politica_a = $('#radio_politica_a');
    let lbl_cola_maxima1 = $('#lbl_cola_maxima1');
    let lbl_cola_maxima2 = $('#lbl_cola_maxima2');
    let lbl_cant_aband1 = $('#lbl_cant_aband1');
    let lbl_cant_aband2 = $('#lbl_cant_aband2');
    let lbl_cant_viajes = $('#lbl_cant_viajes');


    radio_politica_a.prop('checked', true);
    txt.val(8);
    txt.prop('disabled', 'true');


    table = $('#table').DataTable({
        info: false,
        searching: false,
        paginate: false,
        scrollY: '50vh',
        scrollX: true,
        order: false,
        ordering: false,
        processing: true,
        createdRow: function (row, data, dataIndex ) {
            row.setAttribute('onclick', `abrir_modal(${dataIndex})`);
        },
        initComplete: function(){
            let last_row = table.row(':last').data();
            cargar_metricas(last_row, lbl_cola_maxima1, lbl_cola_maxima2, lbl_cant_aband1, lbl_cant_aband2, lbl_cant_viajes);
        },
        ajax: {
            url: 'simular',
            method: 'POST',
            dataSrc: 'data',
            data: function(param) {
                param.c_simulaciones = c_simulaciones.value;
                param.mostrar_desde = mostrar_desde.value;
                param.c_filas_mostrar = c_filas_mostrar.value;
                param.c_autos_traslado = txt.val();
                param.capacidad_vagon = capacidad_vagon.value;
                param.costo_traslado_vagon = costo_traslado_vagon.value;
                param.reposicion_combustible = reposicion_combustible_r.value;
                param.ganancia_traslado_auto = ganancia_traslado_auto.value;
                param.tiempo_vagon_vacio = tiempo_vagon_vacio.value;
                param.tolerancia = tolerancia_espera.value;
                param.costo_abandono = costo_abandono.value;
                param.tiempo_carga_combustible = tiempo_carga_combustible.value;
            },

        },
        columns: [
            {data: 'i'},
            {data: 'reloj'},
            {data: 'name'},
            {data: 'llegada_auto1'},
            {data: 'llegada_auto2'},
            {data: 'abandono_auto1'},
            {data: 'abandono_auto2'},
            {data: 'fin_viaje'},
            {data: 'fin_carga_combustible'},
            {data: 'cantidad_autos_parada1'},
            {data: 'cantidad_autos_parada2'},
            {data: 'ac_costo_traslado_vagon'},
            {data: 'ac_costo_abandono'},
            {data: 'ac_ganancia_traslado_auto'},
            {data: 'estado_vagon'},
            {data: 'nro_viaje_vagon'},
            {data: 'cola_maxima1'},
            {data: 'cola_maxima2'},
            {data: 'cant_abandonos1'},
            {data: 'cant_abandonos2'},
        ],
    });

    $('input[type=radio][name=radio_politica]').change(function() {
        console.log(this.id)
        if (this.id === 'radio_politica_a'){
            txt.val(8)
            txt.prop('disabled', true)
        } else if (this.id === 'radio_politica_b') {
            txt.val(4)
            txt.prop('disabled', true)
        } else if (this.id === 'radio_politica_c') {
            txt.val('')
            txt.prop('disabled', false)
        }

    });

    btn_simular.on('click', function () {
        table.clear();
        table.ajax.reload(function (d){
            let last_row = d['data'][d['data'].length-1];
            cargar_metricas(last_row, lbl_cola_maxima1, lbl_cola_maxima2, lbl_cant_aband1, lbl_cant_aband2, lbl_cant_viajes)
        });
    })
} );

function cargar_metricas(last_row, lbl_cola_maxima1, lbl_cola_maxima2, lbl_cant_aband1, lbl_cant_aband2, lbl_cant_viajes){
    lbl_cola_maxima1.text(last_row['cola_maxima1']);
    lbl_cola_maxima2.text(last_row['cola_maxima2']);
    lbl_cant_aband1.text(last_row['cant_abandonos1']);
    lbl_cant_aband2.text(last_row['cant_abandonos2']);
    lbl_cant_viajes.text(last_row['nro_viaje_vagon']);
}

function abrir_modal(d) {
    console.log();
    let _data = table.row(d).data();
    // data_row_modal.autos1.length = 0;
    // data_row_modal.autos2.length = 0;
    // data_row_modal.autos_en_viaje.length = 0;
    // for (let i = 0; i < _data['autos1'].length; i++){
    //     data_row_modal.autos1.push(_data['autos1'][i])
    // }
    // for (let i = 0; i < _data['autos2'].length; i++){
    //     data_row_modal.autos2.push(_data['autos2'][i])
    // }
    // for (let i = 0; i < _data['autos_en_viaje'].length; i++){
    //     data_row_modal.autos_en_viaje.push(_data['autos_en_viaje'][i])
    // }

    // console.log(data_row_modal);

    let modal_title = `Datos de la Simulación Número: ${_data['i']}`;

    let txt_autos1 = '<ol>';
    for (let i = 0; i < _data['autos1'].length; i++){
        txt_autos1 += `<li>${_data['autos1'][i]}</li>`;
    }
    txt_autos1 += '</ol>';

    let txt_autos2 = '<ol>';
    for (let i = 0; i < _data['autos2'].length; i++){
        txt_autos2 += `<li>${_data['autos2'][i]}</li>`;
    }
    txt_autos2 += '</ol>';

    let txt_autos_en_viaje = '<ol>';
    for (let i = 0; i < _data['autos_en_viaje'].length; i++){
        txt_autos_en_viaje += `<li>${_data['autos_en_viaje'][i]}</li>`;
    }
    txt_autos_en_viaje += '</ol>';

    
    $('#modal_title').text(modal_title);
    $('#modal_autos1').html(txt_autos1);
    $('#modal_autos2').html(txt_autos2);
    $('#modal_autos_en_viaje').html(txt_autos_en_viaje);
    $('#myModal').modal('show');
}
