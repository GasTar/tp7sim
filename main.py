from flask import Flask
from flask import render_template
from flask import request
from tabulate import tabulate

from simulation.sim import Simulation

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/simular', methods=['POST'])
def simular():
    c_iteraciones = int(request.form['c_simulaciones'])
    mostrar_desde = int(request.form['mostrar_desde']) - 1
    c_filas_mostrar = int(request.form['c_filas_mostrar'])
    c_autos_traslado = int(request.form['c_autos_traslado'])
    capacidad_vagon = int(request.form['capacidad_vagon'])
    costo_traslado_vagon = float(request.form['costo_traslado_vagon'])
    reposicion_combustible = int(request.form['reposicion_combustible'])
    tiempo_carga_combustible = float(request.form['tiempo_carga_combustible'])
    tiempo_vagon_vacio = float(request.form['tiempo_vagon_vacio'])
    tolerancia = float(request.form['tolerancia'])
    ganancia_traslado_auto = float(request.form['ganancia_traslado_auto'])
    costo_abandono = float(request.form['costo_abandono'])

    # print((c_iteraciones, mostrar_desde, c_autos_traslado, capacidad_vagon, costo_traslado_vagon,
    #        reposicion_combustible, tiempo_carga_combustible, tiempo_vagon_vacio, tolerancia,
    #        ganancia_traslado_auto, costo_abandono))

    s = Simulation(c_iteraciones, mostrar_desde, c_filas_mostrar, c_autos_traslado, capacidad_vagon, costo_traslado_vagon,
                   reposicion_combustible, tiempo_carga_combustible, tiempo_vagon_vacio, tolerancia,
                   ganancia_traslado_auto, costo_abandono)
    s.simulate()

    return {
        'data': s.data_to_show
    }


# # TESTING
# import time
# start_time = time.time()
#
# c_iteraciones = 50000
# mostrar_desde = 0
# c_autos_traslado = 2  # CAMBIAR 2 por: politica A 4  # politica B 8
# capacidad_vagon = 8
# costo_traslado_vagon = 70
# reposicion_combustible = 8
# tiempo_carga_combustible = 20
# tiempo_vagon_vacio = 3
# tolerancia = 12
# ganancia_traslado_auto = 20
# costo_abandono = 150
#
# s = Simulation(c_iteraciones, mostrar_desde, c_autos_traslado, capacidad_vagon, costo_traslado_vagon,
#                reposicion_combustible, tiempo_carga_combustible, tiempo_vagon_vacio, tolerancia,
#                ganancia_traslado_auto, costo_abandono)
# s.simulate()
#
#
# print(tabulate(s.data_to_show, headers={}))
#
#
# print("time elapsed: {:.2f}s".format(time.time() - start_time))
