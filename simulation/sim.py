from helpers.helpers import truncate
from simulation.generators.Exponential import Exponential
from simulation.generators.Normal import Normal
from simulation.generators.Poisson import Poisson

from simulation.entities.Auto import Auto
from simulation.entities.Vagon import Vagon


PARADA1 = 'En Parada1'
PARADA2 = 'En Parada2'
ENVIAJE12 = 'En viaje12'
ENVIAJE21 = 'En viaje21'
CARGANDOCOMBUSTIBLE1 = 'Cargando Combustible1'
CARGANDOCOMBUSTIBLE2 = 'Cargando Combustible2'

LLEGADA1 = 'Llegada Parada 1'
LLEGADA2 = 'Llegada Parada 2'
ABANDONO1 = 'Abandono 1'
ABANDONO2 = 'Abandono 2'
INICIOVIAJE12 = 'Inicio Viaje 12'
INICIOVIAJE21 = 'Inicio Viaje 21'
FINVIAJE = 'Fin Viaje'
INICIOCARGACOMBUSTILBE = 'Inicio Carga Combustible'
FINCARGACOMBUSTILBE = 'Fin Carga Combustible'


class Simulation:
    def __init__(self, c_simulaciones, mostrar_desde, c_filas_mostrar, c_autos_traslado, capacidad_vagon,
                 costo_traslado_vagon, reposicion_combustible_vagon, tiempo_carga_combustible_vagon, tiempo_vagon_vacio,
                 tolerancia, ganancia_traslado_auto, costo_abandono):

        self.c_simulaciones = c_simulaciones
        self.mostrar_desde = mostrar_desde
        self.c_autos_traslado = c_autos_traslado

        self.vagon = Vagon(PARADA1)

        self.tolerancia_auto = tolerancia
        self.costo_abandono_auto = costo_abandono
        self.ganancia_traslado_auto = ganancia_traslado_auto

        self.costo_traslado_vagon = costo_traslado_vagon
        self.capacidad_vagon = capacidad_vagon
        self.tiempo_carga_combustible_vagon = tiempo_carga_combustible_vagon
        self.reposicion_combustible_vagon = reposicion_combustible_vagon
        self.tiempo_vagon_vacio = tiempo_vagon_vacio

        self.data_to_show = []
        self.number_rows_to_show = c_filas_mostrar

        # init
        self.event_actual = 'Inicialización'
        self.reloj = 0

        self.objetos = {
            'autos1': [],
            'autos2': [],
            'autos_en_viaje': []
        }

        self.events = {
            'llegada_auto1': Llegada1.get_next_arrive(self.reloj),
            'llegada_auto2': Llegada2.get_next_arrive(self.reloj),
            'abandono_auto1': 0,
            'abandono_auto2': 0,
            'fin_viaje': 0,
            'fin_carga_combustible': 0,
        }

        self.acumuladores = {
            'cantidad_autos_parada1': 0,
            'cantidad_autos_parada2': 0,
            'ac_costo_traslado_vagon': 0,
            'ac_ganancia_traslado_auto': 0,
            'ac_costo_abandono': 0,
            'nro_viaje_vagon': 0,
        }

        self.metricas = {
            'cola_maxima1': 0,
            'cola_maxima2': 0,
            'cant_abandonos1': 0,
            'cant_abandonos2': 0,
        }

        # Arranca haciendo viaje 1 -> 2
        # self.inicio_viaje12()

    def add_row_to_show(self, i):
        data = {
            'i': i,
            'reloj': self.reloj,
            'name': self.event_actual,
            **self.events,
            **self.acumuladores,
            **self.metricas,
            'estado_vagon': self.vagon.get_estado(),
            'autos_en_viaje': [],
            'autos1': [],
            'autos2': [],
        }

        for i in range(len(self.objetos['autos_en_viaje'])):
            data['autos_en_viaje'].append(self.objetos['autos_en_viaje'][i].abandono)

        for i in range(len(self.objetos['autos1'])):
            data['autos1'].append(self.objetos['autos1'][i].abandono)

        for i in range(len(self.objetos['autos2'])):
            data['autos2'].append(self.objetos['autos2'][i].abandono)

        self.data_to_show.append(data)

    def do_event(self, e):
        if e == 'llegada_auto1':
            self.llegada1()
        elif e == 'llegada_auto2':
            self.llegada2()
        elif e == 'abandono_auto1':
            self.abandono_auto1()
        elif e == 'abandono_auto2':
            self.abandono_auto2()
        elif e == 'fin_viaje':
            self.fin_viaje()
        elif e == 'fin_carga_combustible':
            self.fin_carga_combustible()
        else:
            raise Exception('Error no se pudo determinar evento')

    def simulate(self):
        rows_showing = 0
        self.add_row_to_show(0)
        for i in range(self.c_simulaciones):
            self.do_event(self.get_next_event())

            if (i >= self.mostrar_desde and rows_showing <= self.number_rows_to_show) or i+1 == self.c_simulaciones:
                self.add_row_to_show(i+1)
                rows_showing += 1

    def llegada1(self):
        self.event_actual = LLEGADA1
        self.reloj = self.events['llegada_auto1']
        self.events['llegada_auto1'] = Llegada1.get_next_arrive(self.reloj)
        self.objetos['autos1'].append(Auto(PARADA1, self.reloj, self.tolerancia_auto))
        self.events['abandono_auto1'] = self.objetos['autos1'][0].abandono
        self.acumuladores['cantidad_autos_parada1'] += 1

        if self.acumuladores['cantidad_autos_parada1'] > self.metricas['cola_maxima1']:
            self.metricas['cola_maxima1'] = self.acumuladores['cantidad_autos_parada1']

        if self.vagon.get_estado() == PARADA1 and len(self.objetos['autos1']) >= self.c_autos_traslado:
            self.inicio_viaje12()

    def llegada2(self):
        self.event_actual = LLEGADA2
        self.reloj = self.events['llegada_auto2']
        self.events['llegada_auto2'] = Llegada2.get_next_arrive(self.reloj)
        self.objetos['autos2'].append(Auto(PARADA2, self.reloj, self.tolerancia_auto))
        self.events['abandono_auto2'] = self.objetos['autos2'][0].abandono
        self.acumuladores['cantidad_autos_parada2'] += 1

        if self.acumuladores['cantidad_autos_parada2'] > self.metricas['cola_maxima2']:
            self.metricas['cola_maxima2'] = self.acumuladores['cantidad_autos_parada2']

        if self.vagon.get_estado() == PARADA2 and len(self.objetos['autos2']) >= self.c_autos_traslado:
            self.inicio_viaje21()

    def abandono_auto1(self):
        self.event_actual = ABANDONO1
        self.reloj = self.events['abandono_auto1']
        self.objetos['autos1'].pop(0)
        self.acumuladores['cantidad_autos_parada1'] -= 1
        self.acumuladores['ac_costo_abandono'] += self.costo_abandono_auto
        self.events['abandono_auto1'] = self.get_next_abandono1()
        self.metricas['cant_abandonos1'] += 1

    def abandono_auto2(self):
        self.event_actual = ABANDONO2
        self.reloj = self.events['abandono_auto2']
        self.objetos['autos2'].pop(0)
        self.acumuladores['cantidad_autos_parada2'] -= 1
        self.acumuladores['ac_costo_abandono'] += self.costo_abandono_auto
        self.get_next_abandono2()
        self.events['abandono_auto2'] = self.get_next_abandono2()
        self.metricas['cant_abandonos2'] += 1

    def inicio_viaje12(self):
        self.event_actual += " | " + INICIOVIAJE12
        self.vagon.set_estado(ENVIAJE12)
        if len(self.objetos['autos1']) > 0:
            while len(self.objetos['autos1']) > 0 and len(self.objetos['autos_en_viaje']) < self.capacidad_vagon:
                self.objetos['autos_en_viaje'].append(self.objetos['autos1'].pop(0))
                self.acumuladores['cantidad_autos_parada1'] -= 1
            self.events['fin_viaje'] = Viaje().get_time(self.reloj)
            self.events['abandono_auto1'] = self.get_next_abandono1()
        else:
            self.events['fin_viaje'] = self.reloj + self.tiempo_vagon_vacio

    def inicio_viaje21(self):
        self.event_actual += " | " + INICIOVIAJE21
        self.vagon.set_estado(ENVIAJE21)

        if len(self.objetos['autos2']) > 0:
            while len(self.objetos['autos2']) > 0 and len(self.objetos['autos_en_viaje']) < self.capacidad_vagon:
                self.objetos['autos_en_viaje'].append(self.objetos['autos2'].pop(0))
                self.acumuladores['cantidad_autos_parada2'] -= 1
            self.events['fin_viaje'] = Viaje().get_time(self.reloj)
            self.events['abandono_auto2'] = self.get_next_abandono2()
        else:
            self.events['fin_viaje'] = self.reloj + self.tiempo_vagon_vacio

    def fin_viaje(self):
        self.event_actual = FINVIAJE
        self.reloj = self.events['fin_viaje']
        self.acumuladores['nro_viaje_vagon'] += 1
        self.acumuladores['ac_costo_traslado_vagon'] += self.costo_traslado_vagon
        self.acumuladores['ac_ganancia_traslado_auto'] += \
            len(self.objetos['autos_en_viaje']) * self.ganancia_traslado_auto
        self.objetos['autos_en_viaje'].clear()

        if self.acumuladores['nro_viaje_vagon'] % self.reposicion_combustible_vagon == 0:
            self.inicio_carga_combustible()
        else:
            self.events['fin_viaje'] = 0
            if self.vagon.get_estado() == ENVIAJE12:
                self.vagon.set_estado(PARADA2)
            elif self.vagon.get_estado() == ENVIAJE21:
                self.vagon.set_estado(PARADA1)

        if self.vagon.get_estado() == PARADA1:
            if len(self.objetos['autos1']) == 0 or len(self.objetos['autos1']) >= self.c_autos_traslado:
                self.inicio_viaje12()
        elif self.vagon.get_estado() == PARADA2:
            if len(self.objetos['autos2']) == 0 or len(self.objetos['autos2']) >= self.c_autos_traslado:
                self.inicio_viaje21()

    def inicio_carga_combustible(self):
        self.event_actual += " | " + INICIOCARGACOMBUSTILBE
        self.reloj = self.events['fin_viaje']
        self.events['fin_carga_combustible'] = self.reloj + self.tiempo_carga_combustible_vagon
        if self.vagon.get_estado() == PARADA1:
            self.vagon.set_estado(CARGANDOCOMBUSTIBLE1)
        else:
            self.vagon.set_estado(CARGANDOCOMBUSTIBLE2)
        self.events['fin_viaje'] = 0

    def fin_carga_combustible(self):
        self.event_actual = FINCARGACOMBUSTILBE
        self.reloj = self.events['fin_carga_combustible']
        self.events['fin_carga_combustible'] = 0

        if self.vagon.get_estado() == CARGANDOCOMBUSTIBLE1:
            self.vagon.set_estado(PARADA1)
            if len(self.objetos['autos1']) == 0 or len(self.objetos['autos1']) >= self.c_autos_traslado:
                self.inicio_viaje12()
        else:  # CARGANDOCOMBUSTIBLE2
            self.vagon.set_estado(PARADA2)
            if len(self.objetos['autos2']) == 0 or len(self.objetos['autos2']) >= self.c_autos_traslado:
                self.inicio_viaje21()

    def get_next_event(self):
        f = True
        next_event = None
        for event in self.events:
            if self.events[event] == 0:
                continue
            if f:
                next_event = event
                f = False
            if self.events[event] < self.events[next_event]:
                next_event = event
        return next_event

    def get_next_abandono1(self):
        return 0 if len(self.objetos['autos1']) == 0 else self.objetos['autos1'][0].abandono

    def get_next_abandono2(self):
        return 0 if len(self.objetos['autos2']) == 0 else self.objetos['autos2'][0].abandono


class Llegada1:
    @staticmethod
    def get_next_arrive(time):
        g = Exponential(1)
        return truncate(time + g.generate(), 4)


class Llegada2:
    @staticmethod
    def get_next_arrive(time):
        p = Poisson(3/5)
        return truncate(time + p.generate(), 4)


class Viaje:
    @staticmethod
    def get_time(time):
        g = Normal(media=5, desviacion=2)
        return truncate(time + g.generate()[0], 4)
