import math
from random import random


class Poisson:

    def __init__(self, _lambda):
        self._lambda = _lambda

    def generate(self):
        p = 1
        x = -1
        a = math.exp(-self._lambda)
        u = random()
        p = p * u
        x = x + 1
        while p >= a:
            u = random()
            p = p * u
            x = x + 1
        # return x
        # TODO: sacar este más uno, lo hice así para que no incluya al cero, pero no debe estar bien
        return x + 1
