from math import log
from random import random


class Exponential:
    #TODO ver si está bien

    def __init__(self, _lambda, media=False):
        if media:
            self._calculate_lambda(media)
        else:
            self._lambda = _lambda

    def generate(self):
        # print(rnd)
        return (-1 / self._lambda) * log(1 - random())

    def _calculate_lambda(self, media):
        self._lambda = 1 / media
