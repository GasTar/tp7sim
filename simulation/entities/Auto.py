from helpers.helpers import truncate


class Auto:
    def __init__(self, estado, time, tolerancia):
        self.estado = estado
        self.abandono = truncate(time + tolerancia, 4)
